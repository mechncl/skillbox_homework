# Домашнее задание №2 для интенсива Skillbox

- [Dockerfile](Dockerfile)
- ["kubectl apply" YAML file](deployment.yml) - *3 replicaset. Deployment и Service в одном файле.*

## Предыдущие задания:
- [Задание #1](https://github.com/mechncl/skillbox_homework1)
- [Задание #1 со звездочкой](https://github.com/mechncl/skillbox_homework1plus)
